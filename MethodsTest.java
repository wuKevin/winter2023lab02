public class MethodsTest
{
	public static void main(String[]args)
	{
		int x = 3;
		int y = 9;
		//double y = 5.5;
		double z = sumSquareRoot(x, y);
		
		String s1 = "java";
		String s2 = "programming";
		
		int test = SecondClass.addOne(50);
		SecondClass sc = new SecondClass();

		
		//methodNoInputNoReturn();
		//methodOneInputNoReturn(x+10);
		//methodTwoInputNoReturn(y, x);
		
		System.out.println(test);
		System.out.println(sc.addTwo(50));
		
		//System.out.println(s1.length());
		//System.out.println(s2.length());
	}
	
	public static void methodNoInputNoReturn()
	{
		System.out.println("I'm a method that takes no input and returns nothing");
		int x = 20;
	}
	
	public static void methodOneInputNoReturn(int test)
	{
		test = test-5;
		System.out.println("Inside the method one input no returns "+test);
	}
	
	public static void methodTwoInputNoReturn(int test1, double test2)
	{
		System.out.println("Inside the method two input no returns "+test1+" and "+test2);
	}
	
	public static int methodNoInputReturnInt()
	{
		return 5;
	}
	
	public static double sumSquareRoot(int a, int b)
	{
		double z = Math.sqrt(a+b);
		return z;
	}
}